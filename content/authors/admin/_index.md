---
# Display name
name: Mir Masood Ali

# Username (this should match the folder name)
authors:
- admin

# Is this the primary user of the site?
superuser: true

# Role/position
role: PhD Student

# Organizations/Affiliations
organizations:
- name: UIC
  url: "https://www.uic.edu/"

# Short bio (displayed in user profile at end of posts)
bio: I conduct research on security and privacy in Internet based systems. 

interests:
- Security and Privacy
- Usability and Trust
- Information Security

education:
  courses:
  - course: MSc in Computer Science
    institution: Dalhousie University
    year: 2019
  - course: BEng in Computer Science and Engineering
    institution: Visvesvaraya Technological University
    year: 2017

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/widgets/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:mali92@uic.edu'  # For a direct email link, use "mailto:test@example.org".
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/__masood__
#- icon: google-scholar
#  icon_pack: ai
#  link: https://scholar.google.co.uk/citations?user=sIwtMXoAAAAJ
- icon: github
  icon_pack: fab
  link: https://github.com/masood
- icon: gitlab
  icon_pack: fab
  link: https://gitlab.com/__masood__
# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.  
- icon: cv
  icon_pack: ai
  link: files/CV_MirMasoodAli.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""
  
# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.  
#user_groups:
#- Researchers
#- Visitors
---

I am a PhD student associated with the Department of Computer Science at the University of Illinois at Chicago, where I'm advised by [Chris Kanich](https://www.cs.uic.edu/~ckanich/). I conduct research on security and privacy in internet based systems. I have a specific focus on the social aspects of usability and trust in online services.

I completed my MSc in Computer Science from Dalhousie University in Aug 2019, where I was supervised by [Dr. Srinivas Sampalli](https://web.cs.dal.ca/~srini).
